from typing import Annotated

from utils.pydantic import SmartRestructuringModel


def test_smart_structure():
    class CommandModel(SmartRestructuringModel):
        interpreter: str = ''
        source: Annotated[str, SmartRestructuringModel.DEFAULT_FIELD] = ''
        flags: str = ''

    class DataModel(SmartRestructuringModel):
        name: str
        id: int
        cmd: list[CommandModel]

    data = {
        'name': 'example',
        'id': 1,
        'cmd': 'ls',
    }

    model = DataModel(**data)
    assert model.model_dump() == {
        'name': 'example',
        'id': 1,
        'cmd': [{'interpreter': '', 'source': 'ls', 'flags': ''}],
    }
