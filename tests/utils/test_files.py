import os
from itertools import chain
from unittest.mock import patch

from utils.files import Strategy, find_files


def test_find_files(fs):
    expected_files = {
        '/Users/test/dev/hello_world/.hw.yaml',
        '/Users/test/dev/hello_world/.hw.yml',
        '/Users/test/dev/.hw.yaml',
        '/Users/test/dev/.hw.yml',
        '/Users/test/.hw.yaml',
        '/Users/test/.hw.yml',
        '/Users/.hw.yaml',
        '/Users/.hw.yml',
        '/.hw.yaml',
        '/.hw.yml',
        '/Users/test/.config/.hw.yaml',
        '/Users/test/.config/.hw.yml',
        '/Users/test/.config/hello_world/.hw.yaml',
        '/Users/test/.config/hello_world/.hw.yml',
        '/Users/test/.config/hello_world/config',
        '/Users/test/.config/hello_world/config.yml',
        '/Users/test/.config/hello_world/config.yaml',
        '/randomfile'
    }

    unrelated_files = {
        '/Users/test/.config/hello_world/config.toml',
    }

    for file in chain(expected_files, unrelated_files):
        fs.create_file(file)

    with patch.dict(os.environ, {'HOME': '/Users/test', 'XDG_CONFIG_HOME': '~/.config'}):
        files = set(
            map(
                str,
                find_files(
                    ('.hw.yaml', '.hw.yml'),
                    file_patterns=r'ra.*le?',
                    strategies=[Strategy.WALK_UP, Strategy.XDG_CONFIG],
                    starting_directory='~/dev/hello_world',
                    xdg_extensions=['', 'yml', 'yaml'],
                    app_name='hello_world',
                ),
            )
        )

    assert not files ^ expected_files
    assert not unrelated_files & files
