#! /usr/bin/env python

from core.config import find_config_files

# ruff: noqa: F821  # Remove when this file is no longer a skeleton


def main() -> None:
    args, unparsed = parse_xcqt_args()
    update_settings(args)
    do_xcqt_things()  # validate taskfile as requested, print things for the user, etc.

    for config_file in find_config_files():
        definitions, config, variables = load_config_file(config_file)
        update_definitions(definitions)
        update_config(config)

    tasks = {}
    while unparsed:
        task_name = unparsed[0]
        task = load_task(task_name)  # parse inline argument definitions
        tasks[task_name] = task
        task.args, unparsed = parse_known_args(task, unparsed)

    # optionally execute tasks in parallel as directed by xcqt settings
    global_context = build_global_context()
    for task in tasks:
        context = build_local_context(task)  # gets called by execute_task instead?
        execute_task(task, global_context, context)


def main2() -> None:
    print('\n'.join(str(file) for file in find_config_files()))


if __name__ == '__main__':
    main2()
