from types import GenericAlias, UnionType
from typing import Any, ClassVar, get_args, get_origin

from pydantic import BaseModel


class SmartRestructuringModel(BaseModel):
    DEFAULT_FIELD: ClassVar[str] = 'default'

    def __init__(self, **data):
        for field_name, field_value in data.items():
            data[field_name] = self._restructure(field_name, field_value)
        super().__init__(**data)

    def _restructure(self, field_name: str, field_value: Any) -> Any:
        field_container, field_type = self._get_origin_and_type(self.__annotations__.get(field_name))

        if issubclass(field_type, BaseModel) and not isinstance(field_value, (BaseModel, dict)):
            default_field = self._get_default_field(field_type)
            if default_field:
                field_value = {default_field: field_value}

        if field_container and issubclass(field_container, list) and not isinstance(field_value, list):
            field_value = [field_value]

        return field_value

    def _get_default_field(self, field_type: type) -> str:
        for field_name, field in field_type.model_fields.items():
            if self.DEFAULT_FIELD in field.metadata:
                return field.alias or field_name

    def _get_origin_and_type(self, annotation: type | GenericAlias | UnionType) -> tuple[type | None, type]:
        origin, args = get_origin(annotation), get_args(annotation)
        return origin, args[0] if args else annotation
