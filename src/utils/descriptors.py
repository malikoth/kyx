from functools import cache, cached_property, update_wrapper
from inspect import isdatadescriptor, ismethoddescriptor
from types import MethodType
from typing import Any, Callable, Self, Type


class classproperty(classmethod):  # noqa: N801
    """A property that is accessible via a class, and not just an object of a class"""

    def __get__(self, obj: Any, cls: Type | None = None):
        return self.__func__(cls or type(obj))


class cachedclassproperty(classproperty):  # noqa: N801
    """A property that is accessible via a class, and not just an object of a class, and is cached"""

    def __init__(self, f: Callable) -> None:
        super().__init__(cache(f))


def is_descriptor(obj: Any) -> bool:
    return ismethoddescriptor(obj) or isdatadescriptor(obj)


def is_property(obj: Any) -> bool:
    return isinstance(obj, (property, cached_property, classproperty, cachedclassproperty))


class descriptordecorator:  # noqa: N801
    def __init__(self, wrapper: Callable, wrapped: Callable | MethodType | None = None) -> None:
        self.wrapper, self.wrapped = wrapper, wrapped
        update_wrapper(self, wrapped)

    def __call__(self, wrapped: Callable | MethodType) -> Self | Callable:
        if ismethoddescriptor(wrapped) or isdatadescriptor(wrapped):
            return self.__class__(self.wrapper, wrapped)
        return self.wrapper(wrapped)

    def __get__(self, obj: object, objtype: type = None) -> Any:
        if isinstance(
            self.wrapped,
            (property, cached_property, classproperty, cachedclassproperty),
        ):
            return self.wrapper(lambda: self.wrapped.__get__(obj, objtype))()
        else:
            return self.wrapper(self.wrapped.__get__(obj, objtype))

    def __getattr__(self, name: str) -> Any:
        return getattr(self.wrapped, name)
