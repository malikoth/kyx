import os
import re
from dataclasses import dataclass
from enum import Enum, auto
from itertools import chain, product
from pathlib import Path
from typing import Iterable, get_origin, get_type_hints

import yaml

from .structures import containerize, is_container


def expanded_path(source: Path | str | None) -> Path | None:
    return Path(source).expanduser().resolve() if source else None


@dataclass
class FileReader:
    filename: Path | str | None = None

    def __post_init__(self):
        if self.filename:
            self.filename = Path(self.filename)

    def as_text(self) -> str:
        if not self.filename:
            return ''

        try:
            return self.filename.read_text()
        except FileNotFoundError:
            return ''

    def as_dict(self) -> dict:
        return yaml.safe_load(self.as_text()) or {}


class XDGMeta(type):
    def __getattribute__(cls, name: str):
        if name.startswith('__'):
            return super().__getattribute__(name)

        value = os.getenv(f'{cls.__name__}_{name}', super().__getattribute__(name))
        origin = get_origin(get_type_hints(cls)[name])
        return list(map(expanded_path, value.split(':'))) if is_container(origin) else expanded_path(value)


class XDG(metaclass=XDGMeta):
    CONFIG_HOME: Path = '~/.config'
    DATA_HOME: Path = '~/.local/share'
    DATA_DIRS: list[Path] = '/usr/local/share/:/usr/share/'
    STATE_HOME: Path = '~/.local/state'


class Strategy(Enum):
    WALK_UP = auto(), None
    WALK_DOWN = auto(), None  # noqa: PIE796
    XDG_CONFIG = auto(), 'config'
    XDG_DATA = auto(), 'data'
    XDG_STATE = auto(), 'state'

    def __init__(self, _, context: str):
        self.context = context


def dirs_for_strategy(strategy: Strategy, directory: None | str | Path = '.') -> list[Path]:
    match strategy:
        case Strategy.WALK_UP:
            return [cwd := expanded_path(directory), *cwd.parents]
        case Strategy.WALK_DOWN:
            return [d for d in expanded_path(directory).iterdir() if d.is_dir()]
        case Strategy.XDG_CONFIG:
            return [XDG.CONFIG_HOME]
        case Strategy.XDG_DATA:
            return [XDG.DATA_HOME, *XDG.DATA_DIRS]
        case Strategy.XDG_STATE:
            return [XDG.STATE_HOME]


def find_files(
    file_names: None | str | Iterable[str] = None,
    file_patterns: None | str | Iterable[str] = None,
    strategies: Strategy | Iterable[Strategy] = Strategy.WALK_UP,
    starting_directory: None | str | Path = '.',
    xdg_extensions: None | str | Iterable[str] = None,
    app_name: None | str = None,
) -> list[Path]:
    paths = {}
    file_names = containerize(file_names)
    for strategy in containerize(strategies):
        directories = dirs_for_strategy(strategy, starting_directory)
        xdg_app_dirs = [d / app_name for d in directories] if app_name and strategy.context else []
        files = product(chain(directories, xdg_app_dirs), file_names)

        if strategy.context and app_name:
            xdg_file_names = (strategy.context + ('.' + ext if ext else ext) for ext in containerize(xdg_extensions))
            xdg_files = product(xdg_app_dirs, xdg_file_names)
            files = chain(files, xdg_files)

        for directory, filename in files:
            if (path := directory / filename).exists():
                paths[path] = None

        for directory, pattern in product(chain(directories, xdg_app_dirs), containerize(file_patterns)):
            if not directory.exists():
                continue

            for file in directory.iterdir():
                if re.match(pattern, file.name):
                    paths[file] = None

    return list(paths)
