from dataclasses import dataclass, field, fields

from .descriptors import cachedclassproperty


@dataclass
class ExpandedDataclass:
    @cachedclassproperty
    def __fields__(cls) -> dict[str, field]:  # noqa: N805
        return {f.name: f for f in fields(cls)}
