from functools import cached_property


class Printable:
    def __str__(self) -> str:
        data = {
            e: getattr(self, e)
            for e in sorted(dir(self))
            if not e.startswith('__') and not callable(getattr(self.__class__, e, None))
        }

        attributes, properties = {}, {}
        property_types = (property, cached_property)
        for k, v in data.items():
            (properties if isinstance(getattr(self.__class__, k, None), property_types) else attributes)[k] = v

        max_width = max(len(k) for k in data)
        lines = [self.__class__.__name__ + ':']
        for name, collection in (
            ('attributes', attributes),
            ('properties', properties),
        ):
            lines.append(f'  {name.capitalize()}:')
            for k, v in collection.items():
                lines.append(f'    {k:>{max_width}}: {v}')

        return '\n'.join(lines)
