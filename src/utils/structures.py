from typing import Any, Callable, Iterable, TypeVar

IterableType = TypeVar('IterableType', bound=Iterable)


def is_container(item: Any) -> bool:
    """Check if an object is a container for other objects.

    Note that "Container" is an overloaded term; The Container ABC does not align with what container means here.
    My definition of container is "iterable that is not also a string or bytes object"

    https://docs.python.org/3/library/collections.abc.html#module-collections.abc
    """
    if isinstance(item, type):
        return issubclass(item, Iterable) and not issubclass(item, (str, bytes))
    else:
        return isinstance(item, Iterable) and not isinstance(item, (str, bytes))


def containerize(item: Any, container_type: None | type = None, sentinel: Any = None) -> type:
    concrete_container_type = container_type or tuple

    if item is sentinel:
        return concrete_container_type()

    if is_container(item):
        if not container_type or isinstance(item, concrete_container_type):
            return item
        return concrete_container_type(item)
    return concrete_container_type((item,))


def filter_dict(collection: dict, condition: Callable = lambda k, v: v is not None) -> dict:
    return {k: v for k, v in collection.items() if condition(k, v)}


def exclude(exclude: Any, source: IterableType) -> IterableType:
    exclude = containerize(exclude)
    return type(source)(element for element in source if element not in exclude)


def multi_get(source: dict, *keys) -> tuple:
    return (source.get(*containerize(key)) for key in keys)
