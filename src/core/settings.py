"""Settings for xcqt

# xcqt
• version
• help

# Configuration
• print-config-files
• list
  • show-all (include private and overridden tasks)
  • sort [declaration | alphabetical]
  • group [taskfile | none]
• validate

• config-scope *
    ○ first (single)
    ○ project
    ○ global
    ○ all (default)
• taskfile

# Tasks and execution
• watch
  • interval
• parallel
• working-dir *
• dry-run -n
• force

# Output
• log-level
• output-coloring [true | auto | false] (auto --> default true, false if outputting to file or no pty)
• output-grouping [true | false]
• output-location [stdout | stderr | <file location> | auto] (auto --> stdout / stderr as appropriate)
• output-prefix [true | auto | false] (auto --> prefix if multiple tasks)
• output-result [all | success | error | none] (success / error implies output-grouping=yes)
"""

from enum import Enum

from pydantic_settings import BaseSettings


class Settings(BaseSettings):
    class ConfigScope(Enum):
        FIRST = 'first'
        PROJECT = 'project'
        GLOBAL = 'global'
        ALL = 'all'

    class Sort(Enum):
        DECLARATION = 'declaration'
        ALPHABETICAL = 'alphabetical'

    class Group(Enum):
        TASKFILE = 'taskfile'
        NONE = 'none'

    config_scope: ConfigScope = ConfigScope.ALL
    print_config_files: bool = False
    list: bool = False
    show_all: bool = False
    sort: Sort = Sort.DECLARATION
    group: Group = Group.TASKFILE
