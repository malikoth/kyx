from pathlib import Path

from utils.files import Strategy, find_files

APP_NAME = 'xcqt'
PROJECT_ROOT_INDICATORS = {
    '.git',
    '.svn',
    '.hg',
    '.p4',
    '.p4config',
    '.bzr',
    '.fslckout',
    '.fossil',
    'pyproject.toml',
}


def find_config_files() -> list[Path]:
    return find_files(
        file_patterns=r'\.?xcqt.*?.ya?ml',
        strategies=[Strategy.WALK_UP, Strategy.XDG_CONFIG],
        xdg_extensions=['', 'yml', 'yaml'],
        app_name='xcqt',
    )


def find_project_root() -> Path | None:
    for directory in (cwd := Path().resolve(), *cwd.parents):
        for file in directory.iterdir():
            if file.name in PROJECT_ROOT_INDICATORS:
                return file.parent


def load_config_file(config_file: Path) -> None:
    pass
