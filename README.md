# xcqt

xcqt is a task executor that treats tasks more like applications with help documentation.

# Inspiration
* task: Original implementation that inspired this project
* tusk: Added the ability to have options and arguments, but has very verbose argument definition
* xc:

# Pseudocode

1. Find config files
2. Load config files
3. Parse command line
   1. Parse specification for task
   2. Build subparser
   3. Parse known args
   4. Repeat
4. Assemble run environment
5. Run commands
   1. Manage parallel execution
   2. Output grouping
   3. Signals
